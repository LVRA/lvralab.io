---
weight: 1
title: Home
bookToC: false
---

# Welcome to the LVRA Wiki

A collection of links, useful resources and guides for the amazing world of VR on Linux.

Feel free to contribute to this wiki yourself if you find anything useful that you might want to share with others.

You can start with the [hardware table](/docs/hardware/).

After which you may want to take a look at the [Envision](/docs/fossvr/envision/) application for quick setup and compatibility with your PC Steam games.