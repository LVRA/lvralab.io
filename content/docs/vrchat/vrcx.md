---
weight: 800
title: VRCX
---

# VRCX
- [VRCX GitHub Repository](https://github.com/vrcx-team/VRCX)

VRCX is a tool for managing your VRChat friendships, as well as providing additional convenience features.

## AppImage (Preferred Method)

### Stable

1. Download the latest .AppImage from here: [VRCX Official Releases](https://github.com/vrcx-team/VRCX/releases)
1. Run `chmod +x VRCX_*.AppImage`.
1. Run the AppImage. This will install it as an app, which then auto-removes the AppImage file.
1. Open VRCX from your apps.

### Nightly

- Do the same steps as above, but download the latest .AppImage from here, instead: [VRCX Nightly Releases](https://github.com/Natsumi-sama/VRCX/releases)

## Installer Script (Wine)

VRCX provides the [install-vrcx.sh](https://github.com/vrcx-team/VRCX/blob/master/Linux/install-vrcx.sh) script upstream, which does the steps of the manual installation for you.

Install it by running: `curl -sSf https://raw.githubusercontent.com/vrcx-team/VRCX/master/Linux/install-vrcx.sh | bash`


## Manual Installation (Wine)

1. Download the latest .zip from here: [VRCX Official Releases](https://github.com/vrcx-team/VRCX/releases)
1. Use a new or existing Wine prefix of Wine 9.2 or later. We recommend using a non-Proton build of Wine.
1. Run `winetricks corefonts`.
1. Symlink your `drive_c/users/steamuser/AppData/LocalLow/VRChat/VRChat` folder from the VRChat Wine prefix to the VRCX Wine prefix.
1. Run `VRCX.exe` via Wine.
