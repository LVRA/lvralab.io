---
title: Outer Wilds VR Mod
weight: 50
---

# Outer Wilds

{{% hint warning %}}

You must use Proton 8 as any other version will not work!

{{% /hint %}}

[Outer Wilds](https://store.steampowered.com/app/753640/Outer_Wilds/) is a flat screen game by design. However, there is a VR mod available to bring greater immersion into the gameplay.

To play Outer Wilds in VR, you need to install [NomaiVR](https://github.com/Raicuparta/nomai-vr).

It is recommended to use the [Outer Wilds Mod Manager](https://outerwildsmods.com/mod-manager/) to install NomaiVR along with any additional mods you choose.

## Performance

Since Outer Wilds was not designed or optimized to be a VR game, performance will be degraded. Expect low framerates in most scenes.

## Bugs

While in VR, if you have mods that add extra items in the main menu, they will be rotated at an odd angle as well as any UIs.
