---
weight: 100
title: WMR Controller Tracking on Arch
---

# Envision Installation on Arch with Controller Tracking

## Recommendations:
- **Paru** as the Arch AUR helper
- **KDE** as the desktop environment for better compatibility with SteamVR (may not be necessary)

### Paru:
[Paru GitHub Repository](https://github.com/Morganamilo/paru)
```bash
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

## Monado Vulkan Layers
These are only needed if you have an NVIDIA Card and a Nvidia Driver before 565.77.01.

[Monado Vulkan Layers AUR](https://aur.archlinux.org/packages/monado-vulkan-layers-git)
### Paru Installation
```bash
paru -S monado-vulkan-layers-git
```
### Normal Installation with compiling manually
```bash
git clone https://aur.archlinux.org/monado-vulkan-layers-git.git
cd monado-vulkan-layers-git
makepkg -i
```
- -> Install dependencies and, if needed, run `makepkg -i` again.

## Envision-XR with Paru
[Envision-XR AUR](https://aur.archlinux.org/packages/envision-xr-git)
```bash
paru -S envision-xr-git
```


### Envision Setup
- Select **WMR default**.

![WMR default](/images/EnvisionXR_WMR.png "Envision XR Screen with WMR Envision Default selected")
- Duplicate and adjust settings:
    - **XR Service Repo**:
    ```bash
    https://gitlab.freedesktop.org/thaytan/monado
    ```
    - **XR Service Branch**:
    ```bash
    dev-constellation-controller-tracking
    ```
    ![WMR adjusted settings](/images/wmr_controller_tracking_envision.png "Repo and Branch adjusted for controller tracking")
- Save and build the profile.
- Plug in the VR headset & turn on the controllers.
- Click **Start** on Monado.
- Once ready, launch a game on Steam.

### Important Notes:
- Do **not** close Envision before starting the game.
- If changing the game:
    - Close the game.
    - Leave Monado open.
    - Start the new game.
- Always turn on the controllers **before** starting Envision.
