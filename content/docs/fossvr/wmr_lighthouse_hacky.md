---
title: WMR Lighthouse (No SteamVR)
weight: 9000
---
# WMR Lighthouse (No SteamVR)

This describes a WMR Lighthouse setup using the `steamvr_lh` driver.

It's different from [WMR Lighthouse](/docs/steamvr/wmr-lighthouse/)

This doesn't use steamvr to re-map the head from some lighthouse tracking device.
Instead it does this natively in monado, so SteamVR is only necessary for the proprietary `steamvr_lh` driver.

For now this is a hacky approach requiring a Monado fork, as Monado's builders don't support the functionality yet:

* https://gitlab.freedesktop.org/monado/monado/-/issues/459
* https://gitlab.freedesktop.org/monado/monado/-/issues/288

This is made for WMR based headsets and has been tested with a Reverb G2 & Vive1 (Vive1 used only for it's 2 watchman dongles needed for the lighthouse controllers).

You can easily adapt this for other headsets in a hacky way, until these issues are closed.

Tested working on Arch Linux with NVIDIA RTX 2060S.

## Setup:
Setup your lighthouse system on SteamVR Windows. The Linux SteamVR version does not work well. On windows: Update everything, pair everything & setup your room boundaries.

Linux Setup:
* install Steam
* Download SteamVR on Steam (try non beta version first)
* Configurations from Windows copied over

### How To Copy Your Windows Steam Configurations To Linux
* mount your windows drive (e.g. at `/mnt`)
* `cp -r "/mnt/Program Files (x86)/Steam/config" ~/.steam/steam/`

### Linux Only Setup
It may be possible to use SteamVR on Linux to setup your room boundaries, pair and update everything. This is untested, but try at least.
You may need playspace moving via Wlx to fine tune the floor if you don't have a Windows install.

## Build

### Manually
Use this fork of Monado

`git clone https://gitlab.freedesktop.org/Zuzka/monado`

(if it's too old for you, try to merge upstream into it and fix it)

Ensure you have all required dependencies installed beforehand (Envision or Monado docs might help)

* `cmake -G Ninja  -B build -DCMAKE_INSTALL_PREFIX=/usr -DXRT_BUILD_DRIVER_SURVIVE=0 -DXRT_BUILD_DRIVER_VIVE=0 -DXRT_BUILD_DRIVER_STEAMVR_LIGHTHOUSE=1`
* `sudo ninja -C build install`

Set following environment variable in your e.g. bash_profile:

`export LH_DEVICE_SERIAL="{YourSerial}"`

Replace `{YourSerial}` with [your actual serial](#how-to-find-your-serial)

### Envision
In Envision, set Profile as such:

XR Service Repo:
```
https://gitlab.freedesktop.org/Zuzka/monado.git
```

XR Service CMake Flags:
```
XRT_BUILD_DRIVER_SURVIVE=0
XRT_BUILD_DRIVER_VIVE=0
XRT_BUILD_DRIVER_STEAMVR_LIGHTHOUSE=1
```
Environment Variables:
```
LH_DEVICE_SERIAL="{YourSerial}"
```

Replace `{YourSerial}` with [your actual serial](#how-to-find-your-serial)

Perform a Clean Build after changing the CMake flags!

## How To Find Your Serial
You can find out the actual tracking device serial by:

1. At monado startup look if you see anything starting with `LHR-` e.g. `LHR-FC2E9BC3`. Try a bunch to find the tracker attached to your headset.
2. running this command and trying a bunch: `ls ~/.steam/steam/config/lighthouse/ | tr '[:lower:]' '[:upper:]'`
3. Running `motoc show` in a terminal

## Notes
* Turn on all your trackers & controllers and ensure the light is green, before starting monado.
* If you decide to use another lighthouse tracker mount, you'd need to figure the new tracker offset math in the Monado source code.
* If you use a Vive 1 as controller connection dongles *only connect power and USB*.
* Fork works fine without a Vive 1 for dongles, usb dongles work aswell.

## Useful Resources:
* https://monado.freedesktop.org/getting-started.html#installation-from-source
* https://monado.freedesktop.org/valve-index-setup.html
* check the commits on the fork for more info https://gitlab.freedesktop.org/Zuzka/monado
* vive tracker 3.0 3d printable mount
  * model: https://www.printables.com/model/107654-hp-reverb-g2-vive-tracker-mount
