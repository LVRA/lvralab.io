---
weight: 800
title: OpenComposite
---

# OpenComposite

- [OpenComposite GitLab repository](https://gitlab.com/znixian/OpenOVR)

> OpenComposite OpenXR (previously known as OpenOVR - OpenVR for OculusVR - but renamed due to confusion with OpenVR) is an implementation of SteamVR's API - OpenVR, forwarding calls directly to the OpenXR runtime. Think of it as a backwards version of ReVive, for the OpenXR compatible headsets.
>
> This allows you to play SteamVR-based games on an OpenXR compatible headset as though they were native titles, without the use of SteamVR!

Please note the OpenVR implementation is incomplete and contains only what's necessary to run most games for compatibility. If you plan to implement software, utilize the OpenXR API, specification [here](https://registry.khronos.org/OpenXR/specs/1.0/html/xrspec.html).

OpenComposite is required for Steam Play games to work in VR regardless of API. Due to the number of confounding factors surrounding setup, sandboxing, and more quirks; we recommend you use Envision to setup and manage your VR.

Contributions to improve the OpenVR to OpenXR mapping are welcome.

## Rebinding Controls

> Changing OpenVR bindings is currently a very manual process. This will change in the future.

We're going to be using VRChat as an example, but this should apply to just about any OpenVR game.

First, we locate where the bindings are stored. For Unity games this is always in `GameName_Data/StreamingAssets/SteamVR`.

In the case of VRChat, it would be in `PATH_TO_STEAM/steamapps/common/VRChat/VRChat_Data/StreamingAssets/SteamVR/`

Create a folder next to the game's executable called `OpenComposite`. Make a copy of the bindings file for your controller and place it in the `OpenComposite` folder. Open it up and look for `controller_type`, it's typically near the bottom of the file.

```json
"controller_type" : "knuckles",
```

Rename the copy you've made to the value of `controller_type`.

In my case i've ended up with a copy of the valve index bindings named `knuckles.json` located in `PATH_TO_STEAM/steamapps/common/VRChat/OpenComposite/knuckles.json`.

Going back to the location of the original bindings, open up `actions.json`, which defines every possible action in the game. A popular binding is changing the microphone button to toggle gestures. We'll use this as an example.

in `actions.json`, we can find the name of the desired actions.
```json
"/actions/Global/in/Mic": "Toggle Microphone", //the one we want to replace
"/actions/Global/in/Gesture_Toggle": "Gesture Toggle", //the one we want to replace it with
```

in our copy `knuckles.json`, we can then look for `actions/Global/in/Mic` and replace it with `Gesture_Toggle` like this:

```json
{
    "inputs" : {
        "click" : {
           "output" : "/actions/global/in/Gesture_Toggle"
        }
    },
    "mode" : "button",
    "path" : "/user/hand/left/input/a"
},
```

And that's it! OpenComposite will automatically pick up these files as long as they're placed and named correctly, and load them instead of the game's provided ones.
